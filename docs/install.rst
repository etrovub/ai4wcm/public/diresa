.. _install:

Install *DIRESA*
================

**Overview**

*DIRESA* is a Python package for dimension reduction based on TensorFlow_.
The distance-regularized Siamese twin autoencoder architecture is designed
to preserve distance (ordering) in latent space while capturing the non-linearities in
the datasets.

.. _TensorFlow: https://www.tensorflow.org


**Install DIRESA**

Install *DIRESA* with the following command:

.. code-block:: bash

  pip install diresa