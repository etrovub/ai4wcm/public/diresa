# *DIRESA*

![test](https://gitlab.com/etrovub/ai4wcm/public/diresa/badges/master/pipeline.svg?ignore_skipped=true&key_text=test&key_width=35)
![release](https://gitlab.com/etrovub/ai4wcm/public/diresa/-/badges/release.svg?key_text=pypi&key_width=35)
![python](https://img.shields.io/badge/python-3.8%20|%203.9%20|%203.10%20|%203.11%20|%203.12-blue)
![tensorflow](https://img.shields.io/badge/tensorflow-2.12%20|%202.13%20|%202.14%20|%202.15%20|%202.16%20|%202.17%20|%202.18-orange)
![mit](https://img.shields.io/badge/license-MIT-yellow)

### Overview

*DIRESA* is a Python package for dimension reduction based on 
[TensorFlow](https://www.tensorflow.org). The distance-regularized 
Siamese twin autoencoder architecture is designed to preserve distance 
(ordering) in latent space while capturing the non-linearities in
the datasets.


### Install *DIRESA*

Install *DIRESA* with the following command:

``` bash
  pip install diresa
```

### Documentation

The *DIRESA* documentation can be found on [Read the Docs](https://diresa-learn.readthedocs.io)